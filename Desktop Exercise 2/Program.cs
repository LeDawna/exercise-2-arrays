﻿using System;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      var pen = ArrayFactory.GetArray();
      
      Console.WriteLine("**** OutputArray ****");
      ArrayFactory.OutputArray(pen);
      
      Console.WriteLine("\r\n**** AverageArrayValue ****");
      var pitcher=ArrayFactory.AverageArrayValue(pen);
      Console.WriteLine("{0:F2}",pitcher);

      Console.WriteLine("\r\n**** MinArrayValue ****");
      var Lowest=ArrayFactory.MinArrayValue(pen);
      Console.WriteLine("{0:F2}",Lowest);

      Console.WriteLine("\r\n**** MaxArrayValue ****");
      var Highest=ArrayFactory.MaxArrayValue(pen);
      Console.WriteLine("{0:F2}",Highest);

      Console.WriteLine("\r\n**** SortArrayAsc ****");
      ArrayFactory.SortArrayAsc(pen);

      Console.Read();
    }
  }
}
