﻿using System;
using System.Linq;

namespace Desktop_Exercise_2
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of 10 random numbers
    /// </summary>
    /// <returns></returns>
    public static decimal[] GetArray()
    {
      var arr = new decimal[10];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween((decimal)0.49, (decimal)149.99);
      }

      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    public static void OutputArray(decimal[] puppies)
    {
      foreach(var bones in puppies)
      {
        Console.WriteLine("{0:F2}",bones);
      }
    }
    

    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    public static decimal AverageArrayValue(decimal[] pitcher)
    {
      var average = (decimal)0;
      foreach(var catcher in pitcher)
      {
        average += catcher;
      }
      average = average / pitcher.Length;
      return average;
    }
      
    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    public static decimal MaxArrayValue(decimal[]pitcher)
    {
      decimal Highest = pitcher.Max();
      return Highest;
    }
    
    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    public static decimal MinArrayValue(decimal[]pitcher)
    {
      decimal Lowest = pitcher.Min();
      return Lowest;
    }
    
    /// <summary>.....
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    /// 
    public static void SortArrayAsc(decimal[]pitcher)
    {
      Array.Sort(pitcher);
      foreach (var catcher in pitcher)
      {
        Console.WriteLine("{0:F2}",catcher);
      }
    }
    
    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}
